<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns="urn:oasis:names:tc:emergency:cap:1.2">
	<xsl:template match="//*[local-name() = 'alert']">
		<xsl:variable name="id"><xsl:value-of select="./*[local-name() = 'identifier']"/></xsl:variable>
		<xsl:variable name="sender"><xsl:value-of select="./*[local-name() = 'sender']"/></xsl:variable>
		<xsl:variable name="sent"><xsl:value-of select="./*[local-name() = 'sent']"/></xsl:variable>
		<xsl:variable name="status"><xsl:value-of select="./*[local-name() = 'status']"/></xsl:variable>
		<xsl:variable name="msgType"><xsl:value-of select="./*[local-name() = 'msgType']"/></xsl:variable>
		<xsl:variable name="scope"><xsl:value-of select="./*[local-name() = 'scope']"/></xsl:variable>
		<xsl:variable name="code"><xsl:value-of select="./*[local-name() = 'code']"/></xsl:variable>
    <xsl:variable name="headline"><xsl:value-of select="./*[local-name() = 'info']/*[local-name() = 'headline']"/></xsl:variable>
    <xsl:variable name="instruction"><xsl:value-of select="./*[local-name() = 'info']/*[local-name() = 'instruction']"/></xsl:variable>
		<xsl:variable name="effective"><xsl:value-of select="./*[local-name() = 'info']/*[local-name() = 'effective']"/></xsl:variable>
    <xsl:variable name="urgency"><xsl:value-of select="./*[local-name() = 'info']/*[local-name() = 'urgency']"/></xsl:variable>
		<xsl:variable name="severity"><xsl:value-of select="./*[local-name() = 'info']/*[local-name() = 'severity']"/></xsl:variable>
		<section>
			<xsl:attribute name="data-cap-identifier">
				<xsl:value-of select="$id"/>
			</xsl:attribute>
			<xsl:attribute name="class">container</xsl:attribute>
			<xsl:attribute name="id">site-content</xsl:attribute>
			<div class="row">
				<div class="col-md-12">
          <h1>
            <xsl:attribute name="style">font-size:48pt;</xsl:attribute>
            <xsl:value-of select="$headline"/>
          </h1>
          <p>
            <xsl:attribute name="style">font-size:38pt;</xsl:attribute>
            <xsl:value-of select="$instruction"/>
          </p>
          <p>Effective:
            <xsl:attribute name="style">font-size:32pt;</xsl:attribute>
            <xsl:value-of select="$effective"/>
          </p>
          <p>Urgency:
            <xsl:attribute name="style">font-size:32pt;</xsl:attribute>
            <xsl:value-of select="$urgency"/>
          </p>
				</div>
			</div>
		</section>
	</xsl:template>

	<xsl:template match="*[local-name() = 'info']">
		<xsl:variable name="language"><xsl:value-of select="./*[local-name() = 'language']"/></xsl:variable>
		<xsl:variable name="category"><xsl:value-of select="./*[local-name() = 'category']"/></xsl:variable>
		<xsl:variable name="event"><xsl:value-of select="./*[local-name() = 'event']"/></xsl:variable>
		<xsl:variable name="responseType"><xsl:value-of select="./*[local-name() = 'responseType']"/></xsl:variable>
		<xsl:variable name="certainty"><xsl:value-of select="./*[local-name() = 'certainty']"/></xsl:variable>
		<xsl:variable name="onset"><xsl:value-of select="./*[local-name() = 'onset']"/></xsl:variable>
		<xsl:variable name="expires"><xsl:value-of select="./*[local-name() = 'expires']"/></xsl:variable>
		<xsl:variable name="senderName"><xsl:value-of select="./*[local-name() = 'senderName']"/></xsl:variable>
		<xsl:variable name="headline"><xsl:value-of select="./*[local-name() = 'headline']"/></xsl:variable>
		<xsl:variable name="description"><xsl:value-of select="./*[local-name() = 'description']"/></xsl:variable>
		<xsl:variable name="web"><xsl:value-of select="./*[local-name() = 'web']"/></xsl:variable>
		<xsl:variable name="contact"><xsl:value-of select="./*[local-name() = 'contact']"/></xsl:variable>
		<xsl:variable name="areaDesc"><xsl:value-of select="./*[local-name() = 'area']/*[local-name() = 'areaDesc']"/></xsl:variable>

	</xsl:template>
</xsl:stylesheet>
